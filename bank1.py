import random
import customer1
from csv import reader, writer, DictReader, DictWriter
import validation1
from pymongo import MongoClient

class Bank:

    client = MongoClient()
    bank_db = client["bank-db"]
    bank_collection = bank_db["bank-collection"]
    customer_db = client["customer-db"]
    customer_collection = customer_db["customer-collection"]
    
    """This is a class bank"""
    def __init__(self,name,initials,address,phone,ifsc_code,branch,email):
        self.name = name
        self.initials = initials
        self.address= address
        self.ifsc_code = ifsc_code
        self.branch = branch
        if validation1.validation.match_phone(phone):
            self.phone = phone
        if validation1.validation.match_email(email):
            self.email = email

    def __repr__(self):
        return f"{self.name}, {self.branch}, {self.initials}"

    def bank_creation(self):
        bank_data = {
            "name": self.name,
            "initials": self.initials,
            "address": self.address,
            "ifsc_code": self.ifsc_code,
            "branch": self.branch,
            "phone": self.phone,
            "email": self.email,
        }
        data_exist = self.bank_collection.find_one({"$or":[{"name":self.name},{"initials": self.initials},{"address": self.address},{"ifsc_code": self.ifsc_code},{"branch": self.branch},
            {"phone": self.phone},{"email": self.email}]})
        if not data_exist:
            self.bank_collection.insert_one(bank_data)
            return "New bank created"
        return "Bank details are already exist"
    


    def get_details(self,name):
        """Get details method will get the details of the bank"""
        data = self.bank_collection.find_one({"name":name})
        if data:
            return f"Please find the bank details: {data}"
        return "No records found"
        

    def update_details(self,new_address,new_phone,new_email,name):
        """Update details can be use to update the details of the bank"""
        if type(new_address)!= str:
            raise TypeError ("Address has to be string")
        if type(new_email)!= str:
            raise TypeError ("Email has to be string")
        data = self.bank_collection.find_one({"name":name})
        if data:
            if self.bank_collection.find_one({"$or":[{"address": new_address},{"phone": new_phone},{"email": new_email}]}):
                return "Details cant be updated please use different Address, Phone & Email"
            else:
                self.bank_collection.update_one({"name":name},{"$set": {"email" : new_email,"phone":new_phone, "address":new_address}})
                return f"Details Updated"
        return f"Bank name dosnt exist"
        
    @classmethod
    def create_customer(cls,first_name,middle_name,last_name,phone,address,email,aadhar_card_no,pan_card_no,bank_name):
        """This method is use to create customer"""
        if type(first_name)!= str:
            raise TypeError ("First Name has to be string")
        if type(last_name)!= str:
            raise TypeError ("Last Name has to be int")
        if type(middle_name)!= str:
            raise TypeError ("Middle Name has to be string")
        if type(address)!= str:
            raise TypeError ("Address has to be string")
        if type(email)!= str:
            raise TypeError ("Email has to be string")
        account_no = random.randint(10000000,20000000)
        balance = 10000
        new_customer = customer1.Customer(first_name,middle_name,last_name,phone,address,email,aadhar_card_no,pan_card_no,account_no,balance,bank_name)
        customer_data = {
            "first_name": new_customer.first_name,
            "middle_name": new_customer.middle_name,
            "last_name": new_customer.last_name,
            "phone": new_customer.phone,
            "address": new_customer.address,
            "email": new_customer.email,
            "aadhar_card_no": new_customer.aadhar_card_no,
            "pan_card_no": new_customer.pan_card_no,
            "account_no": new_customer.account_no,
            "balance": new_customer.balance,
            "bank_name": new_customer.bank_name
        }
        
        bank_cust_exist = cls.bank_collection.find_one({"$or":[{"customers.first_name": new_customer.first_name},{"customers.middle_name": new_customer.middle_name},{"customers.last_name": new_customer.last_name},
        {"customers.phone": new_customer.phone},{"customers.address": new_customer.address},{"customers.email": new_customer.email},
        {"customers.aadhar_card_no": new_customer.aadhar_card_no},{"customers.pan_card_no": new_customer.pan_card_no}]})

        data_exist = cls.customer_collection.find_one({"$or":[{"first_name": new_customer.first_name},{"middle_name": new_customer.middle_name},{"last_name": new_customer.last_name},
        {"phone": new_customer.phone},{"aadhar_card_no": new_customer.aadhar_card_no},{"pan_card_no": new_customer.pan_card_no}]})


        if not bank_cust_exist:
            cls.bank_collection.update_one({"name":bank_name},{"$push": {"customers":customer_data}})
            if not data_exist:
                cls.customer_collection.insert_one(customer_data)
                return new_customer
            else:
                return"Customer already present"
        return "Customer already present in a bank"


    @classmethod    
    def find_customer(cls,account_no,bank_name):
        """This method will find the cutomers"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        
        bank_details = cls.bank_collection.find_one({"name": bank_name})
        if bank_details:
            for customer in bank_details["customers"]:
                if customer["account_no"] == account_no:
                    return f"Please find the customer details: {customer}"            
        return f"No customer found with this account no and in this bank"
        
    @classmethod
    def delete_customer(cls,account_no,bank_name):
        """This method is use to delete the customer"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        
        bank_details = cls.bank_collection.find_one({"name": bank_name})
        data = cls.customer_collection.find_one({"account_no":account_no})
        if bank_details:
            for customer in bank_details["customers"]:
                if customer["account_no"] == account_no:
                    cls.bank_collection.update_one({"name": bank_name},{"$pull": {"customers": {"account_no": account_no}}})
                    if data:
                        cls.customer_collection.delete_one({"account_no":account_no})
                        return f"Deleted customer"
        else:
            return "Account no & bank name not found"

    @classmethod
    def get_all_customer(cls):
        cust_details = []
        """This method is use to get all the customer details"""
        bank_details = cls.bank_collection.find()
        for bank in bank_details:
            cust_details.append(bank["customers"])
        return  "Customers retrieved successfully", cust_details

    @classmethod
    def get_customer_to_csv(cls):
        cust_details = []
        """This method is use to get the customer details and export it to csv"""
        bank_details = cls.bank_collection.find()
        for bank in bank_details:
            cust_details.append(bank["customers"])
        with open("bank.csv", "w") as file:
            csv_writer = writer(file)
            csv_writer.writerow(cust_details)  
        return f"Customer details are added in the csv files"
    
if __name__ == "__main__":
    try:
        print("Bank file will be executed here without any errors")
    except Exception as err:
        print(err)









    

    
