from pprint import pprint
from bson.objectid import ObjectId
from flask import Flask,request,jsonify
from  bank1 import Bank
from customer1 import Customer


app = Flask(__name__)



@app.route("/api/create_bank", methods = ['POST'])
def create_bank():
    bank_data = request.json
    new_bank = Bank(**bank_data)
    response = new_bank.bank_creation()
    return jsonify(response)


@app.route("/api/get_bank", methods = ['POST'])
def get_bank():       
    bank_data = request.json
    bank_name = bank_data.get('name')
    bank_details = Bank.bank_collection.find_one({"name": bank_name})

    if bank_details:
        new_bank = Bank (bank_details["name"],bank_details["initials"],bank_details["address"],bank_details["phone"],bank_details["ifsc_code"],bank_details["branch"],bank_details["email"])
        new_bank.get_details(bank_data["name"])
        response = {"status": "success", "message": f"{new_bank}"}
    else:
        response = {"status": "error", "message": "Bank not found in the database or missing required details."}

    return jsonify(response)


@app.route("/api/update_bank", methods = ['PUT'])
def update_bank():       
    bank_data = request.json
    bank_name = bank_data.get('name')
    bank_details = Bank.bank_collection.find_one({"name": bank_name})

    if bank_details:
        new_bank = Bank (bank_details["name"],bank_details["initials"],bank_details["address"],bank_details["phone"],bank_details["ifsc_code"],bank_details["branch"],bank_details["email"])
        new_bank.update_details(bank_data["new_address"],bank_data["new_phone"],bank_data["new_email"],bank_data["name"])
        response = {"status": "success", "message": f"{new_bank}"}
    else:
        response = {"status": "error", "message": "Bank not found in the database or missing required details."}

    return jsonify(response)


@app.route("/api/create_customer", methods=['POST'])
def create_customer():
    customer_data = request.json
    bank_name = customer_data.get('bank_name')
    bank_details = Bank.bank_collection.find_one({"name": bank_name})

    if bank_details:
        new_cust = Bank.create_customer(customer_data["first_name"],customer_data["middle_name"],customer_data["last_name"],customer_data["phone"],
        customer_data["address"],customer_data["email"],customer_data["aadhar_card_no"],customer_data["pan_card_no"],customer_data["bank_name"])
        response = {"status": "success", "message": f"{new_cust}"}
    else:
        response = {"status": "error", "message": "Bank not found in the database or missing required details."}
    return jsonify(response)


@app.route("/api/find_customer", methods=['POST'])
def find_customer():
    customer_data = request.json
    bank_name = customer_data.get('bank_name')
    bank_details = Bank.bank_collection.find_one({"name": bank_name})

    if bank_details:
        find_cust = Bank.find_customer(customer_data["account_no"],customer_data["bank_name"])
        response = {"status": "success", "message": f"{find_cust}"}
    else:
        response = {"status": "failure", "message": "Bank no not found"}

    return jsonify(response)

@app.route("/api/delete_customer", methods=['DELETE'])
def delete_customer():
    customer_data = request.json
    bank_name = customer_data.get('bank_name')
    bank_details = Bank.bank_collection.find_one({"name": bank_name})

    if bank_details:
        find_cust = Bank.delete_customer(customer_data["account_no"],customer_data["bank_name"])
        response = {"status": "success", "message": f"{find_cust}"}
    else:
        response = {"status": "failure", "message": "Bank no not found"}

    return jsonify(response)


@app.route("/api/get_all_customer", methods=['GET'])
def get_all_customer():
    response = Bank.get_all_customer()
    return jsonify(response)


@app.route("/api/get_all_customer_csv", methods=['GET'])
def get_all_customer_csv():
    response = Bank.get_customer_to_csv()
    return jsonify(response)



@app.route("/api/customers_get_details", methods=['POST'])
def customers_get_details():     
    customer_data = request.json
    customer_account = customer_data.get('account_no')
    customer_details = Bank.customer_collection.find_one({"account_no": customer_account})
    if customer_details:
        new_customer = Customer(customer_details["first_name"],customer_details["middle_name"],customer_details["last_name"],customer_details["phone"],customer_details["address"],customer_details["email"],customer_details["aadhar_card_no"],
                                 customer_details["pan_card_no"],customer_details["account_no"],customer_details["balance"],customer_details["bank_name"])
        new_customer.get_details(customer_data["account_no"])
        response = {"status": "success", "message": f"{new_customer}"}
    else:
        response = {"status": "error", "message": "Customer not found in the database or missing required details."}

    return jsonify(response)

@app.route("/api/customers_update_details", methods=['PUT'])
def customers_update_details():     
    customer_data = request.json
    customer_account = customer_data.get('account_no')
    customer_details = Bank.customer_collection.find_one({"account_no": customer_account})
    if customer_details:
        new_customer = Customer(customer_details["first_name"],customer_details["middle_name"],customer_details["last_name"],customer_details["phone"],customer_details["address"],customer_details["email"],customer_details["aadhar_card_no"],
                                 customer_details["pan_card_no"],customer_details["account_no"],customer_details["balance"],customer_details["bank_name"])
        new_customer.update_details(customer_data["new_address"],customer_data["new_phone"],customer_data["new_email"],customer_data["new_aadhar_card_no"],
                                    customer_data["new_pan_card_no"],customer_data["account_no"])
        response = {"status": "success", "message": f"{new_customer}"}
    else:
        response = {"status": "error", "message": "Customer not found in the database or missing required details."}

    return jsonify(response)


@app.route("/api/customers_get_balance", methods=['POST'])
def customers_get_balance():     
    customer_data = request.json
    customer_account = customer_data.get('account_no')
    customer_details = Bank.customer_collection.find_one({"account_no": customer_account})
    if customer_details:
        new_customer = Customer(customer_details["first_name"],customer_details["middle_name"],customer_details["last_name"],customer_details["phone"],customer_details["address"],customer_details["email"],customer_details["aadhar_card_no"],
                                 customer_details["pan_card_no"],customer_details["account_no"],customer_details["balance"],customer_details["bank_name"])
        new_customer.get_balance(customer_data["account_no"])
        response = {"status": "success", "message": f"{new_customer}"}
    else:
        response = {"status": "error", "message": "Customer not found in the database or missing required details."}

    return jsonify(response)


@app.route("/api/customers_deposite", methods=['POST'])
def customers_deposite():     
    customer_data = request.json
    customer_account = customer_data.get('account_no')
    customer_details = Bank.customer_collection.find_one({"account_no": customer_account})
    if customer_details:
        new_customer = Customer(customer_details["first_name"],customer_details["middle_name"],customer_details["last_name"],customer_details["phone"],customer_details["address"],customer_details["email"],customer_details["aadhar_card_no"],
                                 customer_details["pan_card_no"],customer_details["account_no"],customer_details["balance"],customer_details["bank_name"])
        new_customer.deposite(customer_data["money_to_deposite"],customer_data["account_no"])
        response = {"status": "success", "message": f"{new_customer}"}
    else:
        response = {"status": "error", "message": "Customer not found in the database or missing required details."}

    return jsonify(response)


@app.route("/api/customers_withdraw", methods=['POST'])
def customers_withdraw():     
    customer_data = request.json
    customer_account = customer_data.get('account_no')
    customer_details = Bank.customer_collection.find_one({"account_no": customer_account})
    if customer_details:
        new_customer = Customer(customer_details["first_name"],customer_details["middle_name"],customer_details["last_name"],customer_details["phone"],customer_details["address"],customer_details["email"],customer_details["aadhar_card_no"],
                                 customer_details["pan_card_no"],customer_details["account_no"],customer_details["balance"],customer_details["bank_name"])
        new_customer.withdraw(customer_data["money_to_withdraw"],customer_data["account_no"])
        response = {"status": "success", "message": f"{new_customer}"}
    else:
        response = {"status": "error", "message": "Customer not found in the database or missing required details."}

    return jsonify(response)

if __name__ == "__main__":
    app.run(debug=True)
