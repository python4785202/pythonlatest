from csv import reader, writer, DictReader, DictWriter
import validation1
import bank1


class Customer:
    """This is class customer"""
    def __init__(self,first_name,middle_name,last_name,phone,address,email,aadhar_card_no,pan_card_no,account_no,balance,bank_name):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        if validation1.validation.match_phone(phone):
            self.phone = phone
        if validation1.validation.match_email(email):
            self.email = email
        if validation1.validation.match_aadhar(aadhar_card_no):
            self.aadhar_card_no = aadhar_card_no
        if validation1.validation.match_pan(pan_card_no):
            self.pan_card_no = pan_card_no
        self.address = address
        self.account_no = account_no
        self.balance = balance
        self.bank_name = bank_name

    def __repr__(self):
        return f"{self.first_name}, {self.middle_name}, {self.last_name}, {self.account_no}, {self.balance}"
    
    def get_details(self,account_no):
        """This method of the class customer is use to get the details of the customer"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        data = bank1.Bank.customer_collection.find_one({"account_no":account_no})
        if data:
            return "Please find the customer details:",data
        else:
            return "Customer dosnt exist with this account no"

    def update_details(self,new_address, new_phone, new_email, new_aadhar_card_no, new_pan_card_no, account_no):
        """This method is used to update the details of the customer"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        if type(new_address)!= str:
            raise TypeError ("Address has to be string")
        if type(new_email)!= str:
            raise TypeError ("Email has to be string")
        data = bank1.Bank.customer_collection.find_one({"account_no":account_no})
        # bank_details = bank1.Bank.bank_collection.find_one({"name": bank_name})
        if data:
            data["address"] = new_address
            data["email"] = new_email
            data["phone"] = new_phone
            data["aadhar_card_no"] = new_aadhar_card_no
            data["pan_card_no"] = new_pan_card_no
            bank1.Bank.customer_collection.update_one({"account_no":account_no},{"$set": {"email":data["email"],"address":data["address"],
            "phone":data["phone"],"aadhar_card_no":data["aadhar_card_no"],"pan_card_no":data["pan_card_no"]}})
            bank1.Bank.bank_collection.update_one({"customers.account_no": account_no}, {"$set": {
            "customers.$.email": data["email"],
            "customers.$.address": data["address"],
            "customers.$.phone": data["phone"],
            "customers.$.aadhar_card_no": data["aadhar_card_no"],
            "customers.$.pan_card_no": data["pan_card_no"]
        }})
        
        return f"Updated details" 


    def get_balance(self,account_no):
        """This method is used to get the balance"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        data = bank1.Bank.customer_collection.find_one({"account_no":account_no})
        if data:
            return "Please find the current balance:",data["balance"]



    def deposite(self,money_to_deposite,account_no):
        """Deposite method is used to deposite the money"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        if type(money_to_deposite)!= int:
            raise TypeError ("Money to deposite has to be int")
        data = bank1.Bank.customer_collection.find_one({"account_no":account_no})
        if data:
            data["balance"] += money_to_deposite
            bank1.Bank.customer_collection.update_one({"account_no":account_no},{"$set":{"balance": data["balance"]}})
            bank1.Bank.bank_collection.update_one({"customers.account_no": account_no}, {"$set": {"customers.$.balance":data["balance"]}})
            return f"You have deposited RS: {money_to_deposite}"
        return f"No customer found with account no: {account_no}"


    def withdraw(self,money_to_withdraw,account_no):
        """This method is used to withdraw the money"""
        if type(account_no)!= int:
            raise TypeError ("Account No has to be int")
        if type(money_to_withdraw)!= int:
            raise TypeError ("Money to withdraw has to be int")
        data = bank1.Bank.customer_collection.find_one({"account_no":account_no})
        if data:
            if data["balance"] >= 0 and money_to_withdraw <= data["balance"]:
               data["balance"] -= money_to_withdraw
               bank1.Bank.customer_collection.update_one({"account_no":account_no},{"$set":{"balance": data["balance"]}})
               bank1.Bank.bank_collection.update_one({"customers.account_no": account_no}, {"$set": {"customers.$.balance":data["balance"]}})
        return f"You have withdraw RS: {money_to_withdraw}"


if __name__ == "__main__":
    try:
        print("Customer file will be executed here without any errors")
    except Exception as err:
        print(err)


