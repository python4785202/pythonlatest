
import re

class validation:
    def __init__(self,phone,email,aadhar_card_no,pan_card_no):
        self.phone = phone
        self.email = email
        self.aadhar_card_no = aadhar_card_no
        self.pan_card_no = pan_card_no
    
    @classmethod
    def match_phone(cls,phone):
        pattern = r'\+?\d{0,2}?\s*?\d{10}'
        match = re.match(pattern,phone)
        if match:
            return True
        else:
            raise ValueError("Please enter a valid phone no")

    @classmethod
    def match_email(cls,email):
        pattern = r'(^[a-zA-Z0-9_+.-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9-.]+$)'
        match = re.match(pattern,email)
        if match:
            return True
        else:
            raise ValueError("Please enter a valid email address")

    @classmethod
    def match_aadhar(cls,aadhar_card_no):
        pattern = r'\d{4}\s?\d{4}\s?\d{4}'
        match = re.match(pattern,aadhar_card_no)
        if match:
            return True
        else:
            raise ValueError("Please enter a valid aadhar card no")
        
    @classmethod
    def match_pan(cls,pan_card_no):
        pattern = r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$'
        match = re.match(pattern,pan_card_no)
        if match:
            return True
        else:
            raise ValueError("Please enter a valid pan card no")

